ARG ALPINE_VERSION

FROM alpine:${ALPINE_VERSION}

ARG BUILD_THROTTLE
ARG HEADERS_MORE_VERSION
ARG NGINX_COMMIT
ARG NGINX_VERSION
ARG NGX_BROTLI_COMMIT
ARG NJS_COMMIT
ARG NJS_VERSION
ARG OPENSSL_VERSION
ARG PCRE_VERSION
ARG ZLIB_VERSION

RUN \
  env | sort

# install packages required to prepare sources
RUN \
  apk add --no-cache \
    curl \
    git \
    wget

WORKDIR /usr/src/

RUN \
  echo "Downloading nginx $NGINX_VERSION (rev $NGINX_COMMIT from 'default' branch) ..." \
  && cd /usr/src \
  && wget --no-verbose https://hg.nginx.org/nginx/archive/${NGINX_COMMIT}.tar.gz -O nginx-${NGINX_COMMIT}.tar.gz \
  && tar -xf nginx-${NGINX_COMMIT}.tar.gz

RUN \
  echo "Downloading nginx njs module v${NJS_VERSION} (rev $NJS_COMMIT) ..." \
  && cd /usr/src \
  && wget --no-verbose https://hg.nginx.org/njs/archive/${NJS_COMMIT}.tar.gz -O njs-${NJS_COMMIT}.tar.gz \
  && tar -xf njs-${NJS_COMMIT}.tar.gz

RUN \
  echo "Cloning brotli module (rev $NGX_BROTLI_COMMIT) ..." \
  && mkdir /usr/src/ngx_brotli \
  && cd /usr/src/ngx_brotli \
  && git init \
  && git remote add origin https://github.com/google/ngx_brotli.git \
  && git fetch --depth 1 origin $NGX_BROTLI_COMMIT \
  && git checkout --recurse-submodules -q FETCH_HEAD \
  && git submodule update --init --depth 1

RUN \
  echo "Downloading headers-more module (rev $HEADERS_MORE_VERSION) ..." \
  && cd /usr/src \
  && wget --no-verbose https://github.com/openresty/headers-more-nginx-module/archive/refs/tags/v${HEADERS_MORE_VERSION}.tar.gz -O headers-more-nginx-module.tar.gz \
  && tar -xf headers-more-nginx-module.tar.gz

RUN \
  echo "Downloading zlib (version $ZLIB_VERSION) ..." \
  && cd /usr/src \
  && wget --no-verbose https://zlib.net/fossils/zlib-${ZLIB_VERSION}.tar.gz -O zlib-${ZLIB_VERSION}.tar.gz \
  && tar -xf zlib-${ZLIB_VERSION}.tar.gz

RUN \
  echo "Downloading OpenSSL (version $OPENSSL_VERSION) ..." \
  && cd /usr/src \
  && wget --no-verbose https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz -O openssl-${OPENSSL_VERSION}.tar.gz \
  && tar -xf openssl-${OPENSSL_VERSION}.tar.gz

RUN \
  echo "Downloading PCRE (version $PCRE_VERSION) ..." \
  && cd /usr/src \
  && wget --no-verbose https://github.com/PCRE2Project/pcre2/releases/download/pcre2-${PCRE_VERSION}/pcre2-${PCRE_VERSION}.tar.gz -O pcre2-${PCRE_VERSION}.tar.gz \
  && tar -xf pcre2-${PCRE_VERSION}.tar.gz

RUN \
  echo "Downloading libxslt (version v1.1.37) ..." \
  && cd /usr/src \
  && wget --no-verbose https://gitlab.gnome.org/GNOME/libxslt/-/archive/v1.1.37/libxslt-v1.1.37.tar.gz -O libxslt-v1.1.37.tar.gz \
  && tar -xf libxslt-v1.1.37.tar.gz

# install packages required to build sources
RUN \
  apk add --no-cache \
    autoconf \
    automake \
    gcc \
    geoip-dev \
    libc-dev \
    libtool \
    libxml2-dev \
    libxml2-static \
    libxslt-dev \
    linux-headers \
    make

RUN \
  echo "Calculate MAKE_JOBS ..." \
  && MAKE_JOBS=$(nproc) \
  # Reduce jobs by 4 if there are more then 7 cores or else set jobs to half of core count
  && if [ "$MAKE_JOBS" -ge 8 ]; then export MAKE_JOBS=$(( $MAKE_JOBS - 4 )); else export MAKE_JOBS=$(( $MAKE_JOBS / 2 )); fi \
  # Set jobs to 1 if calculated value is less then 1
  && if [ "$MAKE_JOBS" -lt 1 ]; then export MAKE_JOBS=1; fi \
  # Set jobs back to result of nproc if BUILD_THROTTLE is not requested
  && if [[ "$BUILD_THROTTLE" != "y" ]]; then export MAKE_JOBS=$(nproc); fi \
  && echo "Make job count: $MAKE_JOBS"

# building and installing libxslt from source, since apline does not ship "/usr/lib/libxslt.a" with libxslt-dev and libxslt-static does not exist.
# alpine package contents of libxslt-dev: https://pkgs.alpinelinux.org/contents?branch=v3.17&name=libxslt-dev&arch=x86_64&repo=main
RUN \
  echo "Building libxslt (v1.1.37) ..." \
  && cd /usr/src/libxslt-v1.1.37 \
  && ./autogen.sh \
    --prefix="/usr" \
    --disable-shared \
    --enable-static \
    --without-python \
  && make -j$MAKE_JOBS \
  && make install

ARG NGINX_CONFIGURE_CONFIG="\
  --add-module=/usr/src/headers-more-nginx-module-$HEADERS_MORE_VERSION \
  --add-module=/usr/src/ngx_brotli \
  --add-module=/usr/src/njs-${NJS_COMMIT}/nginx \
  --build=$NGINX_COMMIT \
  --conf-path=/etc/nginx/nginx.conf \
  --error-log-path=/var/log/nginx/error.log \
  --group=nginx \
  --http-client-body-temp-path=/var/cache/nginx/client_temp \
  --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
  --http-log-path=/var/log/nginx/access.log \
  --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
  --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
  --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
  --lock-path=/var/run/nginx/nginx.lock \
  --modules-path=/usr/lib/nginx/modules \
  --pid-path=/var/run/nginx/nginx.pid \
  --prefix=/etc/nginx \
  --sbin-path=/usr/bin/nginx \
  --user=nginx \
  --with-compat \
  --with-debug \
  --with-file-aio \
  --with-http_addition_module \
  --with-http_auth_request_module \
  --with-http_flv_module \
  --with-http_geoip_module \
  --with-http_gunzip_module \
  --with-http_gzip_static_module \
  --with-http_mp4_module \
  --with-http_random_index_module \
  --with-http_realip_module \
  --with-http_secure_link_module \
  --with-http_slice_module \
  --with-http_ssl_module \
  --with-http_stub_status_module \
  --with-http_sub_module \
  --with-http_v2_module \
  --with-http_xslt_module \
  --with-mail \
  --with-mail_ssl_module \
  --with-openssl=/usr/src/openssl-$OPENSSL_VERSION \
  --with-pcre-jit \
  --with-pcre=/usr/src/pcre2-$PCRE_VERSION \
  --with-stream \
  --with-stream_geoip_module \
  --with-stream_realip_module \
  --with-stream_ssl_module \
  --with-stream_ssl_preread_module \
  --with-threads \
  --with-zlib=/usr/src/zlib-$ZLIB_VERSION \
  "

RUN \
  echo "Building nginx ($NGINX_VERSION)..." \
  && cd /usr/src/nginx-$NGINX_COMMIT \
  && ./auto/configure $NGINX_CONFIGURE_CONFIG \
    --with-ld-opt="-static" || cat objs/autoconf.err \
  && make -j$MAKE_JOBS \
  && make install \
  && ls -lah objs

CMD ["/usr/bin/nginx", "-V"]

