# Building
```sh
./build.sh -t docker-nginx-static --no-cache | tee build.log
```

# Start container and get build info
```sh
docker run -it --rm docker-nginx-static:latest
```
