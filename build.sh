#!/bin/sh
set -ex

cat .env | xargs printf -- "--build-arg %s\n" | xargs docker build . $*

